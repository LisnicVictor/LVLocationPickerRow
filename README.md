# LVLocationPickerRow

[![CI Status](http://img.shields.io/travis/Lisnic_Victor/LVLocationPickerRow.svg?style=flat)](https://travis-ci.org/Lisnic_Victor/LVLocationPickerRow)
[![Version](https://img.shields.io/cocoapods/v/LVLocationPickerRow.svg?style=flat)](http://cocoapods.org/pods/LVLocationPickerRow)
[![License](https://img.shields.io/cocoapods/l/LVLocationPickerRow.svg?style=flat)](http://cocoapods.org/pods/LVLocationPickerRow)
[![Platform](https://img.shields.io/cocoapods/p/LVLocationPickerRow.svg?style=flat)](http://cocoapods.org/pods/LVLocationPickerRow)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

LVLocationPickerRow is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "LVLocationPickerRow"
```

## Author

Lisnic_Victor, gerasim.kisslin@gmail.com

## License

LVLocationPickerRow is available under the MIT license. See the LICENSE file for more info.
