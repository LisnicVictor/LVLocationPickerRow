//
//  RowLocaitionPickerViewController.swift
//  Pods
//
//  Created by Victor Lisnic on 5/31/17.
//
//

import Foundation
import LocationPickerViewController
import Eureka
import CoreLocation

public class RowLocationPickerViewController : LocationPicker , TypedRowControllerType {
    public var row: RowOf<RowLocationItem>!
    public var onDismissCallback: ((UIViewController) -> Void)?

    public override func locationDidPick(locationItem: LocationItem) {
        super.locationDidPick(locationItem: locationItem)
        row.value = locationItem.rowLocationItem
        onDismissCallback?(self)
    }

    public override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        super.dismiss(animated: flag, completion: completion)
        onDismissCallback?(self)
    }

    public override func viewDidLoad() {
        super.viewDidLoad()
        guard let item = row.value?.pickerLocationItem else {return}
        selectLocationItem(item)
    }
}
