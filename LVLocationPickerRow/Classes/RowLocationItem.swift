//
//  RowLocationItem.swift
//  Pods
//
//  Created by Victor Lisnic on 6/2/17.
//
//

import Foundation
import MapKit
import LocationPickerViewController

public class RowLocationItem : Equatable {

    public let mapItem : MKMapItem

    public var formattedAddress : String {
        let addressDictionary = mapItem.placemark.addressDictionary
        guard let addressParts = (addressDictionary?["FormattedAddressLines"] as? [String]) else { return "" }
        return addressParts.reduce("", { guard !addressParts.isEmpty , let last = addressParts.last else {return ""}
            let comma = last == $1 ? "" : ", "
            return $0 + $1 + comma
        })
    }

    public init(mapItem: MKMapItem) {
        self.mapItem = mapItem
    }

    public init(coordinate: CLLocationCoordinate2D, addressDictionary: [String: AnyObject]?) {
        let placeMark = MKPlacemark(coordinate: coordinate, addressDictionary: addressDictionary)
        self.mapItem = MKMapItem(placemark: placeMark)
    }
}

public func ==(rhs:RowLocationItem,lhs:RowLocationItem) -> Bool{
    return rhs.mapItem == lhs.mapItem
}

extension LocationItem {
    var rowLocationItem: RowLocationItem {
        return RowLocationItem(mapItem: mapItem)
    }
}

extension RowLocationItem{
    var pickerLocationItem: LocationItem {
        return LocationItem(mapItem: mapItem)
    }
}
