//
//  LocationPIckerRow.swift
//  MeetNShag
//
//  Created by Victor Lisnic on 5/30/17.
//  Copyright © 2017 Vader. All rights reserved.
//

import Foundation
import CoreLocation
import Eureka

public final class DefaultLocationPickerRow : SelectorRow<LocationPickerCell,RowLocationPickerViewController>, RowType {

    public var active : Bool = true {
        didSet{
            cell.isUserInteractionEnabled = active
        }
    }

    public required init(tag: String? = nil) {
        super.init(tag: tag)

        presentationMode = .show(controllerProvider: ControllerProvider.callback {
            return RowLocationPickerViewController()
            }, onDismiss: { vc in
                _ = vc.navigationController?.popViewController(animated: true)
        })

        // Cocoapods
        let podBundle = Bundle(for: DefaultLocationPickerRow.self)
        let bundleURL = podBundle.url(forResource: "LVLocationPickerRow", withExtension: "bundle")
        let bundle = Bundle(url: bundleURL!)!

        cellProvider = CellProvider<LocationPickerCell>(nibName: "LocationPickerCell", bundle: bundle)
    }
}

