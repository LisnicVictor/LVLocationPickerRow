//
//  LocationPickerCell.swift
//  MeetNShag
//
//  Created by Victor Lisnic on 5/30/17.
//  Copyright © 2017 Vader. All rights reserved.
//

import UIKit
import Eureka
import MapKit


open class LocationPickerCell: Cell<RowLocationItem>, CellType {

    @IBOutlet weak var mapView: MKMapView?
    @IBOutlet weak var adressView: UIView?
    @IBOutlet weak var adressLabel: UILabel?

    public lazy var marker: MKPointAnnotation = {
        return MKPointAnnotation()
    }()

    public var distance: Double = 3000

    open override func setup() {
        super.setup()
        mapView?.mapType = .standard
        mapView?.isUserInteractionEnabled = false
        mapView?.addAnnotation(marker)
        adressView?.tintAdjustmentMode = .normal
    }

    open override func update() {
        super.update()
        guard let coordinate = row.value?.mapItem.placemark.coordinate else {return}
        let region = MKCoordinateRegionMakeWithDistance(coordinate, 0 , distance)
        mapView?.setRegion(region, animated: true)
        mapView?.centerCoordinate = coordinate
        adressLabel?.text = row.value?.formattedAddress
        marker.coordinate = coordinate
    }
}
