//
//  ViewController.swift
//  LVLocationPickerRow
//
//  Created by Lisnic_Victor on 06/01/2017.
//  Copyright (c) 2017 Lisnic_Victor. All rights reserved.
//

import UIKit
import Eureka
import LVLocationPickerRow
import CoreLocation


class ViewController: FormViewController {

    var staticLocationItem : RowLocationItem = RowLocationItem(coordinate: CLLocationCoordinate2D(latitude: 45, longitude: 60), addressDictionary: ["FormattedAddressLines":["HoeLand, BitchTown, CuntAve"] as AnyObject])
    var dynamicLocationItem : RowLocationItem?

    override func viewDidLoad() {

        super.viewDidLoad()
        form
            +++ Section()
            <<< DefaultLocationPickerRow(){
                $0.cell.distance = 250000
                $0.value = staticLocationItem
                $0.active = false
                $0.cell.height = {return 150}
            }
            <<< DefaultLocationPickerRow(){
                $0.value = dynamicLocationItem
                $0.cell.height = {return 200}
                $0.cell.selectionStyle = .none
                let _ = $0.onPresent(){
                    $1.addBarButtons()
                    $1.isRedirectToExactCoordinate = false
                }
            }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

